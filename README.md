# Lufty 
Air for the web
---------------


Lufty is a simple, compact  and easy to use CSS Library for paddings, margins, floats, centering and many other useful css classes for html elements.

### The idea behind | _one night_
One night when I was in a rush designing an urgent website prototype, I found that I was using over and over **paddings and margins** attributes to test and adjust the design feeling.

I thought: 

> _"It could be useful to have a library with all those spaces predefined, especially for the initial prototyping"_

...Then later, **lufty was born**.


---


### How it works | _Install_
First, link `lufty.css` or `lufty.min.css` on the head

*Unminified:*

    <link rel="stylesheet" href="lufty.css">

*Minified:*

    <link rel="stylesheet" href="lufty.min.css">

### Start using it
Add any of the [available lufty styles](#lufty-styles) to any element. Use it inside a `class=""` attribute.

*Example:*

    class="blog-item mt-20 mb-15"

This will add a `margin-top:20px;` and `margin-bottom:15px;` to a **`blog-item`**.

See all the available styles [here](#lufty-styles).